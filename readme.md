# Pirate Programming Language
## Info
- `.pirate` files will be compiled

## Usage
- Write a file e.g. `test.pirate`
- run `node interpreter test`
- your code should be executed (interpreted)

## Methods
- `log(string input)` outputs the argument passed to it to the console.
- `addNumbers(...numbers)` sums up all arguments and return the result. Works
    only on numbers, ignores non number arguments. Adds from left to right.

## Syntax
- end lines with a semicolon `;`
