// TODO: remove this file?
const builtInFunctions = {
  addNumbers,
  log
}

function addNumbers(expression) {
  const keywordIndex = expression.findIndex(item => 'addNumbers')
  const openingBracketIndex = expression.findIndex(item => item === '(')
  const closingBracketIndex = expression.findIndex(item => item === ')')
  const functionArguments = expression
    .slice(openingBracketIndex + 1, closingBracketIndex)
    .filter(item => isNumber(item))

  return functionArguments.reduce((sum, item) => sum + Number(item), 0)
}

function log(expression) {
  const keywordIndex = expression.findIndex(item => 'log')
  const openingBracketIndex = expression.findIndex(item => item === '(')
  const closingBracketIndex = expression.findIndex(item => item === ')')

  const loggingInput = expression.slice(openingBracketIndex + 1, closingBracketIndex).join('')
  console.log(loggingInput)
}

function isNumber(item) {
  return item.indexOf('"') === -1 && !isNaN(item)
}

module.exports = builtInFunctions
