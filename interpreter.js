const fs = require('fs')

const builtInFunctions = require('./builtInFunctions')

const FUNCTION_LIST = ['log', 'addNumbers']

const argFileName = process.argv[2] || 'test'
const fileLoaded = fs.readFileSync(`./${argFileName}.pirate`)
const pirateFileName = `${argFileName}.js`

const preparedCode = chain(fileLoaded, [
  String,
  removeNewLine,
  tokenize,
  createExpressions,
  determineFunctionOrder
])

executeCode(preparedCode)

function tokenize(input) {
  const initialAccumulator = {
    partials: [],
    tokens: []
  }

  return input
    .split('')
    .reduce((accumulator, item) => {
      if (item === ' '
        || item === ';'
        || item === '('
        || item === ')'
        || item === ','
      ) {
        accumulator.tokens.push(accumulator.partials.join(''))
        accumulator.tokens.push(item)
        accumulator.partials.splice(0)
      } else {
        accumulator.partials.push(item)
      }
      return accumulator
    }, initialAccumulator).tokens
}

function createExpressions(input) {
  const initialAccumulator = {
    partials: [],
    expressions: []
  }

  return input.reduce((accumulator, item) => {
    if (item === ' ' || item === '') {
      return accumulator
    }

    if (item === ';') {
      accumulator.partials.push(item)
      accumulator.expressions.push([...accumulator.partials])
      accumulator.partials.splice(0)
    } else {
      accumulator.partials.push(item)
    }
    return accumulator
  }, initialAccumulator).expressions
}

function determineFunctionOrder(input) {
  return input.map(splitIntoTree)
}

function splitIntoTree(input) {
  const openingParenthesisIndex = input.reduce((accumulator, item, index) => {
    if (item === '(') {
      accumulator.push(index)
    }
    return accumulator
  }, [])

  const initialTree = {
    child: null,
    parent: null
  }

  return openingParenthesisIndex
    .reverse()
    .reduce((accumulator, item, index) => {
      accumulator = generateTree(accumulator, item, input)
      return accumulator
    }, initialTree)
}

function generateTree(tree, item, input) {
  const clearFunctionSyntax = item => item !== '(' && item !== ')' && item !== ','
  const treeCopy = Object.assign({}, tree)
  if (!treeCopy.child) {
    const firstClosingParenthesisIndex = input.indexOf(')')
    treeCopy.child = input
      .slice(item, firstClosingParenthesisIndex)
      .filter(clearFunctionSyntax)
    treeCopy.parent = {
      child: input.slice(item - 1, item),
      parent: {
        child: null,
        parent: null
      }
    }
  } else {
    const newValue = input
      .slice(item - 1, item)
      .filter(clearFunctionSyntax)
    fillFirstEmptyChild(treeCopy.parent.parent, newValue)
  }

  return treeCopy
}

function fillFirstEmptyChild(treePart, newValue) {
  if (!treePart.child) {
    treePart.child = newValue
    treePart.parent = {
      child: null,
      parent: null
    }
  } else {
    fillFirstEmptyChild(treepart.parent, newValue)
  }
}

// TODO: improve null checks
function executeCode(input) {
  input.forEach(expression => {
    if (isFunction(expression.child && expression.child[0])) {
      runFunction(expression.child)
    } else {
      runNode(expression)
    }
  })
}

function runNode(input) {
  if (input.parent && input.parent.child && isFunction(input.parent.child[0])) {
    const result = {
      child: runFunction(input.parent.child[0], input.child),
      parent: input.parent.parent
    }
    runNode(result)
  } else if (input.parent) {
    runNode(input.parent)
  }
}

function runFunction(name, args) {
  if (name === 'addNumbers') {
    return args.reduce((sum, item) => sum + Number(item), 0)
  } else if (name === 'log') {
    console.log(args)
  }
}

function isFunction(input) {
  return FUNCTION_LIST.indexOf(input) !== -1
}

function removeNewLine(input) {
  let inputCopy = input
  const NEW_LINE = '\r\n'
  const NEW_LINE_REGEX = /\r\n/

  while(inputCopy.includes(NEW_LINE)) {
    inputCopy = inputCopy.replace(NEW_LINE_REGEX, '')
  }

  return inputCopy
}

function chain(input, fns) {
  return fns.reduce((accumulator, fn) => accumulator = fn(accumulator), input)
}
